package com.company.problem_solving_arrays;

import java.util.Scanner;

public class ThreeGroups {
    public static void main(String[] args) {
        Scanner userInput = new Scanner(System.in);
        String[] input = userInput.nextLine().split(" ");

        String[] results = {"", "", ""};

        for (int i = 0; i < input.length; i++) {
            int number = Integer.parseInt(input[i]);
            if (number % 3 == 0) {
                results[0] += number + " ";
            } else if (number % 3 == 1) {
                results[1] += number + " ";
            } else {
                results[2] += number + " ";
            }
        }

        for (int j = 0; j < results.length; j++) {
            if (results[j].length() > 0) {
                System.out.println(results[j].trim());
            } 
        }
    }
}
