package com.company.problem_solving_arrays;

import java.io.ByteArrayInputStream;
import java.util.Scanner;

public class SymmetricArrays {
    //A method with predefined input
   /* private static void testInput(){
        String input = "4\n" +
                "1 2 3 2 1\n" +
                "2 1\n" +
                "1 2 2 1\n" +
                "4";
        System.setIn(new ByteArrayInputStream(input.getBytes()));
    }*/

    public static void main(String[] args) {
        //testInput();

        Scanner userInput = new Scanner(System.in);
        int n = Integer.parseInt(userInput.nextLine());

        for (int i = 0; i < n; i++) {
            boolean isSymmetric = true;
            String[] arr = userInput.nextLine().split(" ");
            for (int j = 0; j < arr.length / 2; j++) {
                if (!arr[j].equals(arr[arr.length - 1 - j])) {
                    isSymmetric = false;
                    break;
                }
            }

            if(isSymmetric){
                System.out.println("Yes");
            }else {
                System.out.println("No");
            }
        }
    }
}
