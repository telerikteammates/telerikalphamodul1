package com.company.problem_solving_arrays;

import java.util.Scanner;

public class ReverseArray {
    public static void main(String[] args) {
        Scanner userInput = new Scanner(System.in);
        String[] input = userInput.nextLine().split(" ");
        String[] revArray = new String[input.length];


        for (int i = 0; i < input.length; i++) {
            revArray[i] = input[input.length - 1 - i];
        }

        System.out.println(String.join(", ", revArray));
    }
}
