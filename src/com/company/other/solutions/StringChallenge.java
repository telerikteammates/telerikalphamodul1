package com.company.other.solutions;

public class StringChallenge {
    public static void main(String[] args) {
        String str = "git is a version <<<<control system>>>> for tracking changes in <<computer files>> and " +
                "coordinating work ot those files among multiple people";
        boolean toUpperCase = false;

        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < str.length(); i++) {
            char c = str.charAt(i);
            if (c == '<') {
                toUpperCase = true;
                continue;
            }

            if (c == '>') {
                toUpperCase = false;
                continue;
            }

            if (toUpperCase) {
                sb.append(Character.toUpperCase(c));
            } else {
                sb.append(c);
            }
        }

        System.out.println(sb);
    }
}

