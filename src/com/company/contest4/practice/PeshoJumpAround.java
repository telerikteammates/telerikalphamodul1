package com.company.contest4.practice;

import java.util.Scanner;

public class PeshoJumpAround {
    public static void main(String[] args) {
        Scanner userInput = new Scanner(System.in);
        String[] intput = userInput.nextLine().split(" ");
        int rows = Integer.parseInt(intput[0]);
        int cols = Integer.parseInt(intput[1]);
        boolean[][] visited = new boolean[rows][cols];
        int jumpsNumber = Integer.parseInt(intput[2]);

        String[] startPosition = userInput.nextLine().split(" ");
        int startRow = Integer.parseInt(startPosition[0]);
        int startCol = Integer.parseInt(startPosition[1]);
        int[] rowSteps = new int[jumpsNumber];
        int[] colSteps = new int[jumpsNumber];

        for (int i = 0; i < jumpsNumber; i++) {
            String[] jumps = userInput.nextLine().split(" ");
            rowSteps[i] = Integer.parseInt(jumps[0]);
            colSteps[i] = Integer.parseInt(jumps[1]);
        }

        boolean isEscaped = false;
        boolean isCaught = false;
        long sum = cols * startRow + startCol + 1;
        visited[startRow][startCol] = true;
        int jumpsCount = 0;
        int currRow = 0;
        int currCol = 0;

        while (!isEscaped & !isCaught) {
            for (int i = 0; i < jumpsNumber; i++) {
                currRow = startRow + rowSteps[i];
                currCol = startCol + colSteps[i];
                if ((currRow > -1 & currRow < rows) & (currCol > -1 & currCol < cols)) {
                    jumpsCount++;
                    if (!visited[currRow][currCol]) {
                        sum = sum + cols * currRow + currCol + 1;
                        visited[currRow][currCol] = true;
                    } else {
                        isCaught = true;
                        break;
                    }
                } else {
                    isEscaped = true;
                    break;
                }
                startRow = currRow;
                startCol = currCol;
               /* System.out.println();
                System.out.println(startRow);
                System.out.println(startCol);
                System.out.println(sum);*/
            }
        }

        if (isEscaped) {
            System.out.println("escaped " + sum);
        }

        if (isCaught) {
            System.out.println("caught " + jumpsCount);
        }
    }
}