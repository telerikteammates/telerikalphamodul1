package com.company.contest4.practice;

import java.util.Arrays;
import java.util.Scanner;

public class MatrixMaxSum {
    public static void main(String[] args) {
        Scanner userInput = new Scanner(System.in);
        int rows = Integer.parseInt(userInput.nextLine());
        String[] row = userInput.nextLine().split(" ");
        int cols = row.length;
        int[][] matrix = new int[rows][cols];


        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < cols; j++) {
                matrix[i][j] = Integer.parseInt(row[j]);
            }
            if (i < rows - 1) {
                row = userInput.nextLine().split(" ");
            }
        }

        String[] positions = userInput.nextLine().split(" ");
        int[] rowPositions = new int[positions.length / 2];
        int[] colPositions = new int[positions.length / 2];

        for (int i = 0; i < positions.length / 2; i++) {
            rowPositions[i] = Integer.parseInt(positions[i * 2]);
            colPositions[i] = Integer.parseInt(positions[i * 2 + 1]);
        }

        //System.out.println(matrix[rows - 1][cols - 1]);
        //System.out.println(rowPositions[1]);
        //System.out.println(colPositions[2]);
        int startRow = 0;
        int endRow = 0;
        int startCol = 0;
        int endCol = 0;
        long sum = 0;


        long[] sums = new long[rowPositions.length];
        for (int i = 0; i < sums.length; i++) {
            startRow = 0;
            endRow = 0;
            startCol = 0;
            endCol = 0;
            if (rowPositions[i] > 0){
                startCol = 0;
                endCol = Math.abs(colPositions[i]) - 1;
            }else{
                startCol = Math.abs(colPositions[i]) - 1;
                endCol = cols - 1;
            }

            for(int ii = startCol; ii <= endCol; ii++){
                sum = sum + matrix[Math.abs(rowPositions[i]) - 1][ii];
            }

            if(colPositions[i] < 0){
                startRow = Math.abs(rowPositions[i]);
                endRow = rows - 1;
            }else{
                startRow = 0;
                endRow = rowPositions[i] * 1 - 2;
            }

            for(int jj = startRow; jj <= endRow; jj++){
                sum = sum + matrix[jj][Math.abs(colPositions[i]) - 1];
            }

            sums[i] = sum;
            //System.out.println(sums[i]);
            sum = 0;
        }

        Arrays.sort(sums);

        System.out.println(sums[sums.length - 1]);
    }
}
