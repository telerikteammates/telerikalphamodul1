package com.company.contest4.practice;

import java.util.Scanner;

public class ScroogeMcDuck {
    public static void main(String[] args) {
        Scanner userInput = new Scanner(System.in);

        String[] matrixSize = userInput.nextLine().split(" ");
        int rows = Integer.parseInt(matrixSize[0]);
        int cols = Integer.parseInt(matrixSize[1]);
        int[][] matrix = new int[rows][cols];
        int currRow = 0;
        int currCol = 0;
        int newRow = 0;
        int newCol = 0;
        boolean isEscaped = false;
        int maxValue = -1;
        int coins = 0;

        for (int i = 0; i < rows; i++) {
            String[] row = userInput.nextLine().split(" ");
            for (int j = 0; j < cols; j++) {
                matrix[i][j] = Integer.parseInt(row[j]);
                if (matrix[i][j] == 0) {
                    currRow = i;
                    currCol = j;
                }
            }
        }

        while (!isEscaped) {
            //Move left
            if (currCol - 1 > -1) {
                if (matrix[currRow][currCol - 1] > maxValue) {
                    newRow = currRow;
                    newCol = currCol - 1;
                    maxValue = matrix[newRow][newCol];
                }
            }

            //Move right
            if (currCol + 1 < cols) {
                if (matrix[currRow][currCol + 1] > maxValue) {
                    newRow = currRow;
                    newCol = currCol + 1;
                    maxValue = matrix[newRow][newCol];
                }
            }

            //Move up
            if (currRow - 1 > -1) {
                if (matrix[currRow - 1][currCol] > maxValue) {
                    newRow = currRow - 1;
                    newCol = currCol;
                    maxValue = matrix[newRow][newCol];
                }
            }

            //Move down
            if (currRow + 1 < rows) {
                if (matrix[currRow + 1][currCol] > maxValue) {
                    newRow = currRow + 1;
                    newCol = currCol;
                    maxValue = matrix[newRow][newCol];
                }
            }

            if (matrix[newRow][newCol] > 0) {
                matrix[newRow][newCol] = matrix[newRow][newCol] - 1;
                coins++;
                currRow = newRow;
                currCol = newCol;
                maxValue = -1;
            } else {
                isEscaped = true;
            }
        }

        System.out.println(coins);
    }
}