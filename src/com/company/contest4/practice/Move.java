package com.company.contest4.practice;

import java.util.Scanner;

public class Move {
    public static void main(String[] args) {
        Scanner userInput = new Scanner(System.in);
        int startPoint = Integer.parseInt(userInput.nextLine());
        String[] valuesArray = userInput.nextLine().split(",");
        int[] values = new int[valuesArray.length];
        int forwardSum = 0;
        int backwardsSum = 0;
        int newPoint = 0;

        for(int i = 0; i < values.length; i++){
            values[i] = Integer.parseInt(valuesArray[i]);
        }

        boolean exitCommand = false;

        while(!exitCommand){
            String input = userInput.nextLine();
            if(!input.equals("exit")){
                String[] commands = input.split(" ");
                int steps = Integer.parseInt(commands[0]);
                String direction = commands[1];
                int size = Integer.parseInt(commands[2]);

                if(direction.equals("forward")){
                    while(steps > 0){
                        newPoint = startPoint + size;
                        if(newPoint < values.length){
                            forwardSum = forwardSum + values[newPoint];
                            steps--;
                            startPoint = newPoint;
                        }else{
                            startPoint = newPoint - values.length - size;
                        }
                    }
                }

                if(direction.equals("backwards")){
                    while(steps > 0){
                        newPoint = startPoint - size;
                        if(newPoint > -1){
                            backwardsSum = backwardsSum + values[newPoint];
                            steps--;
                            startPoint = newPoint;
                        }else{
                            startPoint = values.length + size + newPoint ;
                        }
                    }
                }
            }else{
                exitCommand = true;
            }
            /*System.out.println(forwardSum);
            System.out.println(backwardsSum);*/
        }

        System.out.println("Forward: " + forwardSum);
        System.out.println("Backwards: " + backwardsSum);
    }
}