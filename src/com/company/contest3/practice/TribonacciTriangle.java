package com.company.contest3.practice;

import java.util.Scanner;

public class TribonacciTriangle {
    public static void main(String[] args) {
        Scanner userInput = new Scanner(System.in);
        long firstNumber = Long.parseLong(userInput.nextLine());
        long secondNumber = Long.parseLong(userInput.nextLine());
        long thirdNumber = Long.parseLong(userInput.nextLine());
        int lines = Integer.parseInt(userInput.nextLine());
        long temp = 0;
        String result = "";

        System.out.println(firstNumber);
        System.out.println(secondNumber + " " + thirdNumber);

        if (lines > 2) {
            for (int i = 3; i <= lines; i++) {
                for (int j = 1; j <= i; j++) {
                    temp = firstNumber + secondNumber + thirdNumber;
                    result = result + " " + temp;

                    firstNumber = secondNumber;
                    secondNumber = thirdNumber;
                    thirdNumber = temp;
                }
                System.out.println(result.trim());
                result = "";
            }
        }
    }
}
