package com.company.contest3.practice;

import java.util.Scanner;

public class SpiralMatrix {
    public static void main(String[] args) {
        Scanner userInput = new Scanner(System.in);
        int n = userInput.nextInt();
        int count = 0;

        int currRow = 0;
        int currCol = 0;
        int targetRow = n - 1;
        int targetCol = n - 1;
        int[][] matrix = new int[n][n];
        int startIndex = 0;

        while (count < n * n) {
            currRow = startIndex;
            for (int i = 0 + startIndex; i <= targetCol - startIndex; i++) {
                if (matrix[currRow][i] == 0) {
                    count++;
                    matrix[currRow][i] = count;
                    currCol = i;
                }

                if(count == n * n){
                    break;
                }
            }
            /*System.out.println(currRow);
            System.out.println(currCol);
            System.out.println();*/

            for (int j = 1 + startIndex; j <= targetRow - startIndex; j++) {
                if (matrix[j][currCol] == 0) {
                    count++;
                    matrix[j][currCol] = count;
                    currRow = j;
                }

                if(count == n * n){
                    break;
                }
            }
           /* System.out.println(currRow);
            System.out.println(currCol);
            System.out.println();*/

            for (int ii = currCol - 1; ii >= startIndex; ii--) {
                if (matrix[currRow][ii] == 0) {
                    count++;
                    matrix[currRow][ii] = count;
                    currCol = ii;
                }

                if(count == n * n){
                    break;
                }
            }

            /*System.out.println(currRow);
            System.out.println(currCol);
            System.out.println();*/

            for (int jj = currRow - 1; jj >= startIndex + 1; jj--) {
                if (matrix[jj][currCol] == 0) {
                    count++;
                    matrix[jj][currCol] = count;
                    currRow = jj;
                }

                if(count == n * n){
                    break;
                }
            }

           /* System.out.println(currRow);
            System.out.println(currCol);
            System.out.println();*/

            startIndex++;
        }

        for(int i = 0; i < n; i++){
            for(int j = 0; j < n; j++){
                System.out.print(matrix[i][j]);
                if(j < n - 1){
                    System.out.print(" ");
                }
            }
            System.out.println();
        }
    }
}
