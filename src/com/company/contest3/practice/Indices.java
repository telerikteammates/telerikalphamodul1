package com.company.contest3.practice;

import java.util.Scanner;

public class Indices {
    public static void main(String[] args) {
        Scanner userInput = new Scanner(System.in);
        int count = Integer.parseInt(userInput.nextLine());
        String[] numbers = userInput.nextLine().split(" ");
        boolean[] visited = new boolean[numbers.length];

        boolean outOfBoundary = false;
        int indexValue = 0;
        int index = 0;
        int brackets = 0;
        StringBuilder results = new StringBuilder();
        String result = "";

        while (!outOfBoundary | !visited[index]) {
            index = indexValue;
            indexValue = Integer.parseInt(numbers[index]);
            outOfBoundary = indexValue < 0 | indexValue > numbers.length - 1 | indexValue > 2000000000;
            if (!outOfBoundary) {
                if (!visited[index]) {
                    results.append(index + " ");
                    visited[index] = true;
                } else {
                    brackets = 1;
                    break;
                }
            } else {
                results.append(index);
                break;
            }
        }

        if (brackets != 0) {
            int firstPosition = results.indexOf(String.valueOf(index));
            if (index == 0) {
                //char str = '(';
                results.insert(0, '(');
            } else {
                results.setCharAt(firstPosition - 1, '(');
            }
            int lastPosition = results.length() - 1;
            results.setCharAt(lastPosition, ')');
        }

        // Convert to string.
        result = results.toString();

        // Print result.
        System.out.println(result);
    }
}