package com.company.oop;

public class Point {
    private double coordX;
    private double coordY;


    //default constructor
    public Point(){
        /*this.coordX = 0;
        this.coordY = 0;*/
        this(0.0);
    }

    //constructor one parameter
    public Point(double coordXY){
        /*this.coordX = coordXY;
        this.coordY = coordXY;*/
        this(coordXY, coordXY);
    }

    //constructor two parameters
    public Point(double coordX, double coordY){
        this.coordX = coordX;
        this.coordY = coordY;
    }


    //getter
    public double getCoordX(){
        return coordX;
    }


    //setter
    public void setCoordX(double x){
        coordX = x;
    }

    //getter
    public void setCoordY(double y){
        coordX = y;
    }

    //setter
    public double getCoordY(){
        return coordY;
    }

    void printCoordinates(){
        System.out.println("Point (" + coordX + "," + coordY + ")" );
        System.out.printf("Point (%.1f,%.1f)",  coordX, coordY);
        System.out.println();
    }

    void  translate(double x, double y){
        this.coordX =+ x;
        this.coordY =+ y;
        //System.out.printf("Point (%.1f,%.1f)", coordX + 2, coordY + 3);
    }
}
