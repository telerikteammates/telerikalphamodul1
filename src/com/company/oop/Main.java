package com.company.oop;

public class Main {
    public static void main(String[] args) {
        /*Point newPoint = new Point();
        newPoint.coordX = 6;
        newPoint.coordY = 6;

        newPoint.printCoordinates();*/

        Point defaultPoint = new Point();
        defaultPoint.printCoordinates();
        defaultPoint.translate(3,6);
        System.out.println();

        Point singlePoint = new Point(9.9);
        singlePoint.printCoordinates();
        singlePoint.translate(2,3);
        System.out.println();

        Point doublePoint = new Point(3,9);
        doublePoint.printCoordinates();
        doublePoint.translate(doublePoint.getCoordX(),doublePoint.getCoordY());
        System.out.println();

        doublePoint.setCoordX(13);
        doublePoint.printCoordinates();

        double test = doublePoint.getCoordX();
        System.out.println(test);
    }
}
