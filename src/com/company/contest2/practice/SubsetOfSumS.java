package com.company.contest2.practice;

import java.util.Scanner;

public class SubsetOfSumS {
    public static void main(String[] args) {
        Scanner userInput = new Scanner(System.in);
        long sum = Integer.parseInt(userInput.nextLine());
        String[] input = userInput.nextLine().split(" ");
        long subset, nextNumber;
        boolean output = false;

        outerloop:
        for (int i = 0; i < input.length; i++) {
            output = false;
            subset = Long.parseLong(input[i]);
            if (subset < sum) {
                for (int j = i + 1; j < input.length; j++) {
                    nextNumber = Long.parseLong(input[j]);
                    subset += nextNumber;
                    if (subset > sum) {
                        subset = subset - nextNumber;
                    } else if (subset == sum) {
                        output = true;
                        break outerloop;
                    }
                }
            } else if (subset == sum) {
                output = true;
                break outerloop;
            }
        }

        if (output) {
            System.out.println("yes");
        } else {
            System.out.println("no");
        }
    }
}