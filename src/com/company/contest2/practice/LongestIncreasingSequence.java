package com.company.contest2.practice;

import java.util.Scanner;

public class LongestIncreasingSequence {
    public static void main(String[] args) {
        Scanner userInput = new Scanner(System.in);
        int numbers = userInput.nextInt();
        int number = userInput.nextInt();
        int count = 1;
        int longestSequence = 0;

        for(int i = 1; i < numbers; i++){
            int nextNumber = userInput.nextInt();
            if(nextNumber > number){
                count++;
            }else{
                count = 1;
            }
            number = nextNumber;

            if (count >= longestSequence) {
                longestSequence = count;
            }
        }

        System.out.println(longestSequence);

    }
}
