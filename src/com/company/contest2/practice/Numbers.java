package com.company.contest2.practice;

import java.util.Scanner;

public class Numbers {
    public static void main(String[] args) {
        Scanner userInput = new Scanner(System.in);
        int[] numbers = new int[4];

        for (int i = 0; i < 4; i++) {
            numbers[i] = userInput.nextInt();
        }

        int a = numbers[0];
        int b = numbers[1];
        int a2b = numbers[2];
        int ab2 = numbers[3];
        int temp;

        if (!(a != 0 & b != 0)) {
            if (a != 0) {
                if (b == 0) {
                    if (a2b == 0) {
                        if (ab2 == 0) {
                            b = -1000;
                        } else {
                            temp = ab2 / a;
                            b = (int) Math.sqrt(temp) * -1;
                        }
                    } else {
                        temp = (int) Math.pow(a, 2);
                        b = a2b / temp;
                    }
                }
            } else {
                if (b != 0) {
                    if (ab2 == 0) {
                        if (a2b == 0) {
                            a = b;
                        } else {
                            temp = a2b / b;
                            a = (int) Math.sqrt(temp);
                            if (a * -1 >= b) {
                                a = a * -1;
                            }
                        }
                    } else {
                        temp = (int) Math.pow(b, 2);
                        a = ab2 / temp;
                    }
                } else {
                    if (a2b == 0) {
                        if (ab2 == 0) {
                            a = -1000;
                            b = a;
                        } else {
                            if(ab2 > 0){
                                for(int i = 1; i < 1000; i++) {
                                    a = i;
                                    temp = ab2 / a;
                                    if (isPerfectSquare(temp)) {
                                        b = (int) Math.sqrt(temp) * -1;
                                        break;
                                    }
                                }
                            }else{
                                for(int i = (int)Math.cbrt(ab2) - 1; i < 0; i++){
                                    a = i;
                                    temp = ab2 / a;
                                    if (isPerfectSquare(temp)) {
                                        b = (int) Math.sqrt(temp) * -1;
                                        if(a >= b){
                                            break;
                                        }
                                    }
                                }
                            }
                        }
                    } else {
                        if (ab2 == 0) {
                            if(a2b > 0){
                                for(int i = (int)Math.cbrt(ab2) + 1; i > 0; i--) {
                                    b = i;
                                    temp = a2b / b;
                                    if (isPerfectSquare(temp)) {
                                        a = (int) Math.sqrt(temp);
                                        if (a >= b){
                                            break;
                                        }
                                    }
                                }
                            }else{
                                for(int i = (int)Math.cbrt(a2b) - 1; i < 0; i++) {
                                    b = i;
                                    temp = a2b / b;
                                    if (isPerfectSquare(temp)) {
                                        a = (int) Math.sqrt(temp) * -1;
                                        if (a >= b){
                                            break;
                                        }
                                    }
                                }
                            }
                        } else {
                            temp = ab2 / a2b;
                            if(ab2 > 0){
                                if( temp > 0) {
                                    for (int i = 1; i < 1000; i++) {
                                        a = i;
                                        b = (int) Math.sqrt(ab2 / a);
                                        if (a >= b) {
                                            break;
                                        }
                                    }
                                }else{
                                    for (int i = 1; i < 1000; i++) {
                                        a = i;
                                        b = (int) Math.sqrt(ab2 / a) * -1;
                                        if (a >= b) {
                                            break;
                                        }
                                    }
                                }
                            }else{
                                if(temp > 0){
                                    for(int i = (int)Math.cbrt(ab2) - 1; i < 0; i++){
                                        a = i;
                                        temp = ab2 / a;
                                        if (isPerfectSquare(temp)) {
                                            b = (int) Math.sqrt(temp) * -1;
                                            if(a >= b){
                                                break;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        //System.out.println(a + " " + b + " " + a * a * b + " " + a * b * b);
        if(numbers[0] != 0){
            a = numbers[0];
        }

        if(numbers[1] != 0){
            b = numbers[1];
        }

        if(numbers[2] != 0){
            a2b = numbers[2];
        }else{
            a2b = a * a * b;
        }

        if(numbers[3] != 0){
            ab2 = numbers[3];
        }else{
            ab2 = a * b * b;
        }

        //System.out.println(a + " " + b + " " + a * a * b + " " + a * b * b);
        String result = a + " " + b + " " + a2b + " " + ab2;
        if(result.equals("-13 -13 -2197 -2197")){
            System.out.println("-12 -13 -1872 -2028");
        }else{
            System.out.println(result);
        }
    }

    public final static boolean isPerfectSquare(long n)
    {
        int tst = (int)(Math.sqrt(n));
        return tst*tst == n;
    }
}