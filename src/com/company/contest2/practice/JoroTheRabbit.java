package com.company.contest2.practice;

    import java.util.Scanner;

    public class JoroTheRabbit {
        public static void main(String[] args) {
            Scanner userInput = new Scanner(System.in);
            String[] input = userInput.nextLine().split(", ");
            int[] terrain = new int[input.length];
            int startPosition = 0;
            int stepSize = 0;
            int jumps = 0;
            int nextPosition = 0;
            int maxJumps = 0;
            int jumpAround = 0;

            for (int i = 0; i < input.length; i++) {
                terrain[i] = Integer.parseInt(input[i]);
            }

            while (jumpAround < terrain.length) {
                for (int i = 1; i < terrain.length; i++) {
                    startPosition = jumpAround;
                    stepSize = i;
                    jumps = 1;
                    nextPosition = startPosition + stepSize;
                    if (nextPosition > terrain.length - 1) {
                        nextPosition = nextPosition - terrain.length;
                    }

                    while (terrain[nextPosition] > terrain[startPosition]) {
                        jumps++;
                        startPosition = nextPosition;
                        nextPosition = startPosition + stepSize;
                        if (nextPosition > terrain.length - 1) {
                            nextPosition = nextPosition - terrain.length;
                        }
                    }

                    if (jumps > maxJumps) {
                        maxJumps = jumps;
                        jumps = 1;
                    }
                }

                jumpAround++;
            }

            System.out.println(maxJumps);
        }
    }