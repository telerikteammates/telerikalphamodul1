package com.company.contest2.practice;

import java.util.Scanner;

public class ZigZag {
    public static void main(String[] args) {
        Scanner userInput = new Scanner(System.in);
        int n = userInput.nextInt();
        int m = userInput.nextInt();
        long position;
        long sum = 0;

        for(int i = 0; i < n; i++){
            for(int j = 0; j < m; j++){
                if(i == 0) {
                    if (j % 2 == 0) {
                        position = 1 + i * 3 + j * 3;
                        sum += position;
                    }
                }else if(i > 0 & i < n - 1){
                    if(i % 2 != 0 & j % 2 != 0){
                        if(j != m - 1){
                            position = 1 + i * 3 + j * 3;
                            sum += position * 2;
                        }else{
                            position = 1 + i * 3 + j * 3;
                            sum += position;
                        }
                    }
                    if(i % 2 == 0 & j % 2 ==0){
                        if(j != 0){
                            position = 1 + i * 3 + j * 3;
                            sum += position * 2;
                        }else{
                            position = 1 + i * 3 + j * 3;
                            sum += position;
                        }
                    }
                }else{
                    if(i % 2 == 0 & j % 2 == 0){
                        position = 1 + i * 3 + j * 3;
                        sum += position;
                    }else if(i % 2 != 0 & j % 2 !=0){
                        position = 1 + i * 3 + j * 3;
                        sum += position;
                    }
                }
            }
            //System.out.println(sum);
        }

        System.out.println(sum);
    }
}
