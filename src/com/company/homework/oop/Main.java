package com.company.homework.oop;

import java.util.ArrayList;

public class Main {
    public static void main(String[] args) {
        ArrayList<Person> people = new ArrayList<>();

        Person pesho = new Person("Pesho", 23, "0887123456");
        pesho.addInterest("Football");
        pesho.addInterest("Computer Games");
        people.add(pesho);

        Person gosho = new Person("Gosho", 30, "0887654321");
        gosho.addInterest("Ski");
        gosho.addInterest("Climbing");
        gosho.addInterest("Programming");
        people.add(gosho);

        Person ivana = new Person("Ivana", 21, "0887112233");
        ivana.addInterest("Books");
        ivana.addInterest("Cooking");
        people.add(ivana);

        Person misho = new Person("Misho", 25, "0888888888");
        misho.addInterest("Music");
        misho.addInterest("Play drums");
        misho.addInterest("Yoga");
        people.add(misho);

        Person rumi = new Person("Rumi", 27, "0885588111");
        rumi.addInterest("Programming");
        rumi.addInterest("Dancing");
        people.add(rumi);

        pesho.addFriend(gosho);
        pesho.addFriend(misho);

        gosho.addFriend(rumi);

        ivana.addFriend(pesho);
        ivana.addFriend(rumi);

        misho.addFriend(pesho);
        misho.addFriend(gosho);
        misho.addFriend(ivana);
        misho.addFriend(rumi);

        rumi.addFriend(ivana);

        Person didi = new Person("Didi", 17, "0879000608");
        people.add(didi);

        for (Person person : people) {
            person.introduce();
            person.sharePhone();
            person.showFriends();

            System.out.println();
            System.out.println();
        }
    }
}