package com.company.oopprinciples.inheritance.users;

public class Nurse extends Staff {
    public Nurse(long id, String firstName, String lastName, String gender, String email,
                 double salary, boolean isInsured, int age, boolean smoking, int yearsOfExperience, String description) {
        super(id, firstName, lastName, gender, email, salary, isInsured, age, smoking, yearsOfExperience, description);
    }
}
