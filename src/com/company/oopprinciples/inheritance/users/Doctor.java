package com.company.oopprinciples.inheritance.users;

public class Doctor extends Staff {
    private String specialisation;

    public Doctor(long id, String firstName, String lastName, String gender, String email,
                  double salary, boolean isInsured, int age, boolean smoking, int yearsOfExperience, String description, String specialisation) {
        super(id, firstName, lastName, gender, email, salary, isInsured, age, smoking, yearsOfExperience, description);
        this.specialisation = specialisation;
    }

    public String getSpecialisation() {
        return specialisation;
    }

    public void setSpecialisation(String specialisation) {
        this.specialisation = specialisation;
    }
}
