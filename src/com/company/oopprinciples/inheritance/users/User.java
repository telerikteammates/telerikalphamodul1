package com.company.oopprinciples.inheritance.users;

import com.company.oopprinciples.inheritance.insurance.plan.HealthInsurancePlan;

public abstract class User {
    private long id;
    private String firstName;
    private String lastName;
    private String gender;
    private String email;
    private double salary;
    private boolean isInsured;
    //Composition example
    private HealthInsurancePlan insurancePlan;
    private int age;
    private boolean smoking;

    public User(long id, String firstName, String lastName, String gender, String email,
                double salary, boolean isInsured, int age, boolean smoking) {
        setId(id);
        setFirstName(firstName);
        setLastName(lastName);
        setGender(gender);
        setEmail(email);
        setSalary(salary);
        setInsured(isInsured);
        setAge(age);
        setSmoking(smoking);
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public boolean isInsured() {
        return isInsured;
    }

    public void setInsured(boolean insured) {
        isInsured = insured;
    }

    public HealthInsurancePlan getInsurancePlan() {
        return insurancePlan;
    }

    public void setInsurancePlan(HealthInsurancePlan insurancePlan) {
        this.insurancePlan = insurancePlan;
    }

    public double getSalary() {
        return salary;
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public boolean isSmoking() {
        return smoking;
    }

    public void setSmoking(boolean smoking) {
        this.smoking = smoking;
    }
}
