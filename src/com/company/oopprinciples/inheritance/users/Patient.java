package com.company.oopprinciples.inheritance.users;

public class Patient extends User {
    public Patient(long id, String firstName, String lastName, String gender, String email,
                   double salary, boolean isInsured, int age, boolean smoking) {
        super(id, firstName, lastName, gender, email, salary, isInsured, age, smoking);
    }
}
