package com.company.oopprinciples.inheritance.users;

public class Staff extends User {
    private int yearsOfExperience;
    private String description;


    public Staff(long id, String firstName, String lastName, String gender, String email,
                 double salary, boolean isInsured, int age, boolean smoking, int yearsOfExperience, String description) {
        super(id, firstName, lastName, gender, email, salary, isInsured, age, smoking);
        this.yearsOfExperience = yearsOfExperience;
        this.description = description;
    }

    public int getYearsOfExperience() {
        return yearsOfExperience;
    }

    private void setYearsOfExperience(int yearsOfExperience) {
        this.yearsOfExperience = yearsOfExperience;
    }

    public String getDescription() {
        return description;
    }

    private void setDescription(String description) {
        this.description = description;
    }
}
