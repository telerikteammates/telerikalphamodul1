package com.company.oopprinciples.inheritance.billing;

import com.company.oopprinciples.inheritance.insurance.plan.HealthInsurancePlan;
import com.company.oopprinciples.inheritance.users.Patient;

public class Billing {

    public static double[] computePaymentAmount(Patient patient, double amount) {
        double[] payments = new double[2];

        payments[0] = 0;
        payments[1] = amount;
        HealthInsurancePlan healthInsurancePlan = patient.getInsurancePlan();
        double coverage;
        if (patient.getInsurancePlan() != null) {
            coverage = healthInsurancePlan.getCoverage();
            payments[0] = amount * coverage / 100;
            payments[1] = amount - payments[0];
        }

        double discount = 20;
        if (patient.getInsurancePlan() != null) {
            discount = patient.getInsurancePlan().getDiscount();
        }

        payments[1] = payments[1] - discount;
        if (payments[1] < 0) {
            payments[1] = 0;
        }

        return payments;
    }
}
