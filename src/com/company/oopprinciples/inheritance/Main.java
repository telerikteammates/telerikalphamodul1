package com.company.oopprinciples.inheritance;

import com.company.oopprinciples.inheritance.insurance.companies.BlueCrossBlueShield;
import com.company.oopprinciples.inheritance.insurance.companies.InsuranceBrand;
import com.company.oopprinciples.inheritance.insurance.plan.*;
import com.company.oopprinciples.inheritance.billing.Billing;
import com.company.oopprinciples.inheritance.users.*;

public class Main {
    public static void main(String[] args) {
        InsuranceBrand insuranceBrand = new BlueCrossBlueShield();
        HealthInsurancePlan platinumPlan = new PlatinumPlan();
        HealthInsurancePlan goldPlan = new GoldPlan();
        HealthInsurancePlan silverPlan = new SilverPlan();
        HealthInsurancePlan bronzePlan = new BronzePlan();
        HealthInsurancePlan[] plans = {platinumPlan,goldPlan,silverPlan,bronzePlan};

        for (HealthInsurancePlan plan : plans) {
            plan.setOfferedBy(insuranceBrand);
        }

        double[] payments;
        double computeMonthlyPremium;

        //Staff object
        User staff = new Staff(1,"Platinum", "Staff", "staff",
                "staff@123.123", 0,true,70,true, 30, "description");
        staff.setInsurancePlan(platinumPlan);
        computeMonthlyPremium = platinumPlan.computeMonthlyPremium(staff.getSalary(), staff.getAge(), staff.isSmoking());
        System.out.printf("The monthly premium for user %s %s is: $%.2f.\n\n",staff.getFirstName(),staff.getLastName(),computeMonthlyPremium);


        //Patient object - Platinum plan
        Patient platinumPatient = new Patient(1,"Platinum", "Patient", "platinum",
                "platinum@123.123", 5000,true,48,true);
        platinumPatient.setInsurancePlan(platinumPlan);
        payments = Billing.computePaymentAmount(platinumPatient, 1000);
        computeMonthlyPremium = platinumPlan.computeMonthlyPremium(platinumPatient.getSalary(), platinumPatient.getAge(), platinumPatient.isSmoking());
        System.out.printf("Total payment by the patient: $%.2f\n", payments[1]);
        System.out.printf("The monthly premium for user %s %s is: $%.2f.\n\n",platinumPatient.getFirstName(),platinumPatient.getLastName(),computeMonthlyPremium);


        //Patient object - Gold plan
        Patient goldPatient = new Patient(1,"Gold", "Patient", "gold",
                "gold@123.123",1500,true,59,false);
        goldPatient.setInsurancePlan(goldPlan);
        payments = Billing.computePaymentAmount(goldPatient, 1000);
        computeMonthlyPremium = goldPlan.computeMonthlyPremium(goldPatient.getSalary(), goldPatient.getAge(), goldPatient.isSmoking());
        System.out.printf("Total payment by the patient: $%.2f\n", payments[1]);
        System.out.printf("The monthly premium for user %s %s is: $%.2f.\n\n",goldPatient.getFirstName(),goldPatient.getLastName(),computeMonthlyPremium);

        //Patient object - Silver plan
        Patient silverPatient = new Patient(1,"Silver", "Patient", "silver",
                "silver@123.123", 10000,true,101,true);
        silverPatient.setInsurancePlan(silverPlan);
        payments = Billing.computePaymentAmount(silverPatient, 1000);
        computeMonthlyPremium = silverPlan.computeMonthlyPremium(silverPatient.getSalary(), silverPatient.getAge(), silverPatient.isSmoking());
        System.out.printf("Total payment by the patient: $%.2f\n", payments[1]);
        System.out.printf("The monthly premium for user %s %s is: $%.2f.\n\n",silverPatient.getFirstName(),silverPatient.getLastName(),computeMonthlyPremium);


        //Patient object - Silver plan
        Patient bronzePatient = new Patient(1,"Bronze", "Patient", "bronze",
                "bronze@123.123", 300,true,25,true);
        bronzePatient.setInsurancePlan(bronzePlan);
        payments = Billing.computePaymentAmount(bronzePatient, 1000);
        computeMonthlyPremium = bronzePlan.computeMonthlyPremium(bronzePatient.getSalary(), bronzePatient.getAge(), bronzePatient.isSmoking());
        System.out.printf("Total payment by the patient: $%.2f\n", payments[1]);
        System.out.printf("The monthly premium for user %s %s is: $%.2f.\n\n",bronzePatient.getFirstName(),bronzePatient.getLastName(),computeMonthlyPremium);

        //Patient object - no plan
        Patient uninsuredPatient = new Patient(1,"Test", "Patient", "test",
                "test@123.123",0,false,18,false);
        payments = Billing.computePaymentAmount(uninsuredPatient, 1000);
        System.out.printf("Total payment by the patient: $%.2f\n", payments[1]);
    }
}
