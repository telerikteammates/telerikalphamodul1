package com.company.oopprinciples.inheritance.insurance.companies;

import com.company.oopprinciples.inheritance.insurance.plan.HealthInsurancePlan;

public interface InsuranceBrand {
   double computeMonthlyPremium(HealthInsurancePlan insurancePlan, int age, boolean smoking);
}