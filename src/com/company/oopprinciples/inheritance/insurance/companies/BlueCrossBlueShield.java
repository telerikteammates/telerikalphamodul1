package com.company.oopprinciples.inheritance.insurance.companies;

import com.company.oopprinciples.inheritance.insurance.plan.HealthInsurancePlan;

public class BlueCrossBlueShield implements InsuranceBrand {
    private long id;
    private String name;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BlueCrossBlueShield() {

    }

    public BlueCrossBlueShield(long id, String name) {
        this.id = id;
        this.name = name;
    }

    @Override
    public double computeMonthlyPremium(HealthInsurancePlan insurancePlan, int age, boolean smoking) {
        double monthlyPremium = 0;
        String plan = insurancePlan.getPlanName();

        if (age > 55) {
            switch (plan) {
                case "Platinum":
                    monthlyPremium += 200;
                    break;
                case "Gold":
                    monthlyPremium += 150;
                    break;
                case "Silver":
                    monthlyPremium += 100;
                case "Bronze":
                    monthlyPremium += 50;
                    break;
                default:
                    monthlyPremium += 0;
            }
        }

        if (smoking) {
            switch (plan) {
                case "Platinum":
                    monthlyPremium += 100;
                    break;
                case "Gold":
                    monthlyPremium += 90;
                    break;
                case "Silver":
                    monthlyPremium += 80;
                case "Bronze":
                    monthlyPremium += 70;
                    break;
                default:
                    monthlyPremium += 0;
            }
        }

        return monthlyPremium;
    }
}
