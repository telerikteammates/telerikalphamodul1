package com.company.oopprinciples.inheritance.insurance.plan;

public class GoldPlan extends HealthInsurancePlan {
    private static final int COVERAGE = 80;
    private static final int DISCOUNT = 40;
    private static final int PREMIUM = 7;
    public static final String GOLD = "Gold";

    public GoldPlan(){
        setPlanName(GOLD);
        setCoverage(COVERAGE);
        setDiscount(DISCOUNT);
        setPremium(PREMIUM);
    }

    public GoldPlan(String planName, double coverage, double discount, double premium) {
        super(planName, coverage, discount, premium);
        setPlanName(planName);
    }

    @Override
    public double computeMonthlyPremium(double salary, int age, boolean smoking) {
        double monthlyPremium = salary * PREMIUM / 100 + getOfferedBy().computeMonthlyPremium(this,age,smoking);
        return monthlyPremium;
    }
}
