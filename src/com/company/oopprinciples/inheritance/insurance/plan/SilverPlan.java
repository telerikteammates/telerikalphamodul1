package com.company.oopprinciples.inheritance.insurance.plan;

public class SilverPlan extends HealthInsurancePlan {
    private static final int COVERAGE = 70;
    private static final int DISCOUNT = 30;
    private static final int PREMIUM = 7;
    public static final String SILVER = "Silver";

    public SilverPlan(){
        setPlanName(SILVER);
        setCoverage(COVERAGE);
        setDiscount(DISCOUNT);
        setPremium(PREMIUM);
    }

    public SilverPlan(String planName, double coverage, double discount, double premium) {
        super(planName, coverage, discount, premium);
        setPlanName(planName);
    }

    @Override
    public double computeMonthlyPremium(double salary, int age, boolean smoking) {
        double monthlyPremium = salary * PREMIUM / 100 + getOfferedBy().computeMonthlyPremium(this,age,smoking);
        return monthlyPremium;
    }


}
