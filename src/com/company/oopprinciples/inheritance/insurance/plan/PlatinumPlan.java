package com.company.oopprinciples.inheritance.insurance.plan;

public class PlatinumPlan extends HealthInsurancePlan{
    private static final int COVERAGE = 90;
    private static final int DISCOUNT = 50;
    private static final int PREMIUM = 8;
    public static final String PLATINUM = "Platinum";

    public PlatinumPlan(){
        setPlanName(PLATINUM);
        setCoverage(COVERAGE);
        setDiscount(DISCOUNT);
        setPremium(PREMIUM);
    }

    public PlatinumPlan(String planName, double coverage, double discount, double premium) {
        super(planName, coverage, discount, premium);
        setPlanName(planName);
    }

    @Override
    public double computeMonthlyPremium(double salary, int age, boolean smoking) {
        double monthlyPremium = salary * PREMIUM / 100 + getOfferedBy().computeMonthlyPremium(this,age,smoking);
        return monthlyPremium;
    }
}
