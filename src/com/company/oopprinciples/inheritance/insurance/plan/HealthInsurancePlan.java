package com.company.oopprinciples.inheritance.insurance.plan;

import com.company.oopprinciples.inheritance.insurance.companies.InsuranceBrand;

public abstract class HealthInsurancePlan{
    private String planName;
    private double coverage;
    private double discount;
    private double premium;
    private InsuranceBrand offeredBy;

    public String getPlanName() {
        return planName;
    }

    public void setPlanName(String planName) {
        this.planName = planName;
    }

    public double getCoverage() {
        return coverage;
    }

    public void setCoverage(double coverage) {
        this.coverage = coverage;
    }

    public double getDiscount() {
        return discount;
    }

    public void setDiscount(double discount) {
        this.discount = discount;
    }

    public double getPremium() {
        return premium;
    }

    public void setPremium(double premium) {
        this.premium = premium;
    }

    public InsuranceBrand getOfferedBy() {
        return offeredBy;
    }

    public void setOfferedBy(InsuranceBrand offeredBy) {
        this.offeredBy = offeredBy;
    }

    public HealthInsurancePlan(){

    }

    public HealthInsurancePlan(String planName, double coverage, double discount, double premium) {
        setPlanName(planName);
        setCoverage(coverage);
        setDiscount(discount);
        setPremium(premium);
    }

    public abstract double computeMonthlyPremium(double salary, int age, boolean smoking);
}
