package com.company.contest1.practice;

import java.io.ByteArrayInputStream;
import java.math.BigInteger;
import java.util.Scanner;

public class BitShiftMatrix2 {
    private static void testInput() {
        String input = "5\n" +
                "6\n" +
                "4\n" +
                "14 27 1 5";

        System.setIn(new ByteArrayInputStream(input.getBytes()));
    }

    public static void main(String[] args) {
        //testInput();
        Scanner userInpit = new Scanner(System.in);
        int r = Integer.parseInt(userInpit.nextLine());
        int c = Integer.parseInt(userInpit.nextLine());
        int n = Integer.parseInt(userInpit.nextLine());

        int[] targetCells = new int[n];
        String[] targetCellsStr = userInpit.nextLine().split(" ");
        for (int i = 0; i < n; i++) {
            targetCells[i] = Integer.parseInt(targetCellsStr[i]);
        }

        BigInteger[][] matrix = new BigInteger[r][c];
        BigInteger startElement = BigInteger.valueOf(1);

        for (int row = r - 1; row >= 0; row--) {
            matrix[row][0] = startElement;
            for (int col = 1; col < c; col++) {
                matrix[row][col] = matrix[row][col - 1].multiply(BigInteger.valueOf(2));
            }
            startElement = startElement.multiply(BigInteger.valueOf(2));
        }

        int coef = Math.max(r, c);
        int currRow = r - 1;
        int currCol = 0;
        BigInteger sum = BigInteger.valueOf(0);


        for (int targetCell : targetCells) {
            int targetRow = targetCell / coef;
            int targetCol = targetCell % coef;

            int hDirection = 1;
            if (currCol > targetCol) {
                hDirection = -1;
            }

            int vDirection = 1;
            if (currRow > targetRow) {
                vDirection = -1;
            }

            while (currCol != targetCol) {
                sum = sum.add(matrix[currRow][currCol]);
                matrix[currRow][currCol] = BigInteger.valueOf(0);
                currCol += hDirection;
            }

            while (currRow != targetRow) {
                sum = sum.add(matrix[currRow][currCol]);
                matrix[currRow][currCol] = BigInteger.valueOf(0);
                currRow += vDirection;
            }
        }

        sum = sum.add(matrix[currRow][currCol]);

        System.out.println(sum);
    }
}
