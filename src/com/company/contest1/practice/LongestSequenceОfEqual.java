package com.company.contest1.practice;

import java.util.ArrayList;
import java.util.Scanner;

public class LongestSequenceОfEqual {
    public static void main(String[] args) {
        Scanner userInput = new Scanner(System.in);
        int n = Integer.parseInt(userInput.nextLine());
        ArrayList<String> numbers = new ArrayList<>();

        int count = 1;
        int longestSequence = 0;
        numbers.add(userInput.nextLine());

        for (int i = 1; i < n; i++) {
            numbers.add(userInput.nextLine());
            if (numbers.get(i).equals(numbers.get(i - 1))) {
                count++;
            } else {
                count = 1;
            }

            if (count > longestSequence) {
                longestSequence = count;
            }
        }

        System.out.println(longestSequence);

    }
}

