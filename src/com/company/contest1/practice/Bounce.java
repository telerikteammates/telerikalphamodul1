package com.company.contest1.practice;

import java.math.BigInteger;
import java.util.Scanner;

public class Bounce {
    public static void main(String[] args) {
        Scanner userInput = new Scanner(System.in);
        int n = userInput.nextInt();
        int m = userInput.nextInt();
        int currRow = 1;
        int currCol = 1;
        int newRow = 1;
        int newCol = 1;
        BigInteger sum = BigInteger.valueOf(1);


        while (n != 1 & m != 1) {
            sum = sum.add(BigInteger.valueOf(2).pow(currRow + currCol));

            if (isCorner(currRow, currCol, n, m)) {
                break;
            }

            if (currRow == 0 | currRow == n - 1) {
                newRow *= -1;
            }

            if (currCol == 0 | currCol == m - 1) {
                newCol *= -1;
            }

            currRow += newRow;
            currCol += newCol;
        }

        System.out.println(sum);
    }

    public static boolean isCorner(int row, int col, int n, int m) {
        boolean isCorner = false;

        if ((row == 0 | row == n - 1) & (col == 0 | col == m - 1)) {
            isCorner = true;

        }

        return isCorner;
    }
}
