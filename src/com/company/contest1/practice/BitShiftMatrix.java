package com.company.contest1.practice;

import java.math.BigInteger;
import java.util.Scanner;

public class BitShiftMatrix {
    public static void main(String[] args) {
        Scanner userInput = new Scanner(System.in);
        int rows = Integer.parseInt(userInput.nextLine());
        int cols = Integer.parseInt(userInput.nextLine());
        int moves = Integer.parseInt(userInput.nextLine());
        String[] codes = userInput.nextLine().split(" ");

        int coeff = Math.max(rows, cols);
        int code, newRow, newCol;
        int currRow = rows - 1;
        int currCol = 0;

        boolean[][] matrix = new boolean[rows][cols];

        BigInteger sum = BigInteger.valueOf(0);

        for (int i = 0; i < codes.length; i++) {
            code = Integer.parseInt(codes[i]);
            newRow = code / coeff;
            newCol = code % coeff;

            if (currCol < newCol) {
                for (int j = currCol; j <= newCol; j++) {
                    if (!matrix[currRow][j]) {
                        sum = sum.add(BigInteger.valueOf(2).pow(rows - 1 - currRow + j));
                        matrix[currRow][j] = true;
                        //System.out.println(sum);
                    }
                }
            } else if (currCol > newCol) {
                for (int k = newCol; k <= currCol; k++) {
                    if (!matrix[currRow][k]) {
                        sum = sum.add(BigInteger.valueOf(2).pow(rows - 1 - currRow + k));
                        matrix[currRow][k] = true;
                        //System.out.println(sum);
                    }
                }
            }

            currCol = newCol;

            if (currRow < newRow) {
                for (int jj = currRow; jj <= newRow; jj++) {
                    if (!matrix[jj][currCol]) {
                        sum = sum.add(BigInteger.valueOf(2).pow(rows - 1 - jj + currCol));
                        matrix[jj][currCol] = true;
                        //System.out.println(sum);
                    }
                }
            } else if (currRow > newRow) {
                for (int kk = newRow; kk <= currRow; kk++) {
                    if (!matrix[kk][currCol]) {
                        sum = sum.add(BigInteger.valueOf(2).pow(rows - 1 - kk + currCol));
                        matrix[kk][currCol] = true;
                        //System.out.println(sum);
                    }
                }
            }

            currRow = newRow;
            //System.out.println(sum);

        }

        System.out.println(sum);
    }
}
