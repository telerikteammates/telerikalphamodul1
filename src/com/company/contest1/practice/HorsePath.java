package com.company.contest1.practice;

import java.util.Scanner;

public class HorsePath {
    public static void main(String[] args) {
        Scanner userInput = new Scanner(System.in);
        int n = userInput.nextInt();

        int[][] matrix = new int[n][n];
        int currRow = 0;
        int currCol = 0;
        matrix[0][0] = 1;
        int count = 1;
        int[] rowMoves = {-2, -2, -1, -1, 1, 1, 2, 2};
        int[] colMoves = {-1, 1, -2, 2, -2, 2, -1, 1};
        int newRowPosition;
        int newColPosition;
        boolean isInRange;

        while (count < n * n) {
            isInRange = false;
            for (int i = 0; i < 8; i++) {
                newRowPosition = currRow + rowMoves[i];
                newColPosition = currCol + colMoves[i];

                if (newRowPosition >= 0 & newRowPosition <= n - 1 & newColPosition >= 0 & newColPosition <= n - 1) {
                    if (matrix[newRowPosition][newColPosition] == 0) {
                        count++;
                        currRow = newRowPosition;
                        currCol = newColPosition;
                        matrix[currRow][currCol] = count;
                        isInRange = true;
                        break;
                    }
                }
            }

            if (!isInRange) {
                outerloop:
                for (int ii = 0; ii < n; ii++) {
                    for (int jj = 0; jj < n; jj++) {
                        if (matrix[ii][jj] == 0) {
                            currRow = ii;
                            currCol = jj;
                            count++;
                            matrix[currRow][currCol] = count;
                            break outerloop;
                        }
                    }
                }
            }
        }

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                System.out.print(matrix[i][j] + " ");
            }
            System.out.println();
        }
    }
}